import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';


var PLAYERS = [
    {
        id: 1,
        name: 'Vaishali',
        score: 30
    },
    {
        id: 2,
        name: 'Aditi',
        score: 50
    },
    {
        id: 3,
        name: 'Divya',
        score: 10
    },
    {
        id: 4,
        name: 'Deepanshu',
        score: 40
    },
];
var nextId = 5;
ReactDOM.render(<App initialPlayers={PLAYERS} title='My Scoreboard' nextId={nextId}/>, document.getElementById('root'));
registerServiceWorker();