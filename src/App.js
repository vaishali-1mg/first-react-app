import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './App.css';
import Header from './components/Header/Header';
import Player from './components/Player/Player';
import AddPlayer from './components/AddPlayer/AddPlayer';
// import DeletePlayer from './components/DeletePlayer/DeletePlayer';

class App extends Component {
  constructor(props){
    super(props);
    this.state={
      players: this.props.initialPlayers,
      nextId: this.props.nextId
    }
  }
  onScoreChange=(index,delta)=>{
    this.state.players[index].score+=delta;
    this.setState(this.state);
  }
  onPlayerAdd=(name)=>{
    this.state.players.push({
      id:this.state.nextId,
      name: name,
      score:0
    });
    this.setState(this.state);
    this.state.nextId+=1;
  }
  render() {
    return (
      <div className="scoreboard">

        {/* HEADER */}
        <Header title={this.props.title} players={this.props.initialPlayers}/>

        {/* PLAYER Using loops in jsx  */}
        <div className="players">
          {
            this.state.players.map((player,index)=>{
              return(
                <Player 
                  key={player.id} 
                  name={player.name} 
                  score={player.score} 
                  onScoreChange={(delta)=>{this.onScoreChange(index,delta)}}/>
              )
            })
          }
        </div>
        {/* ADD NEW PLAYER */}
        <AddPlayer onAdd={this.onPlayerAdd}/>
      </div>
    );
  }
}

App.propTypes = {
  title: PropTypes.string.isRequired,
  initialPlayers: PropTypes.arrayOf(PropTypes.shape({
  name: PropTypes.string.isRequired,
  score: PropTypes.number.isRequired
  })).isRequired
};
export default App;
