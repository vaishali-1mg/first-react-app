import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './AddPlayer.css';

class AddPlayer extends Component {
    constructor(props){
        super(props);
        this.state = {
            name:'',
        }
    }

    onSubmit=()=>{
        this.props.onAdd(this.state.name);
        this.setState({name:''});
    }
    onNameChange=(event)=>{
        this.setState({
            name:event.target.value
        })
    }
    render() {
        return (
            <div className="add-player-form">
                <form>
                    <input type='text'  value={this.state.name} className='add-player' onChange={this.onNameChange}/>
                    <input type='submit' value='Add Player' className='submit-player' onClick={this.onSubmit}/>
                </form>
            </div>   
        );
    }
}
AddPlayer.propTypes = {
    onAdd: PropTypes.func.isRequired
};
export default AddPlayer;
