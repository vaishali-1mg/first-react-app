import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Stats.css';

class Stats extends Component {

    
    render() {
        var totalPlayers = this.props.players.length;
        var totalScore = this.props.players.reduce((total,player)=>{
            return total+player.score;
        },0);
        return (
            <table className='stats'>
                <tbody>
                    <tr>
                        <td>Players: </td>
                        <td>{totalPlayers}</td>
                    </tr>

                    <tr>
                        <td>Total Points: </td>
                        <td>{totalScore}</td>
                    </tr>
                </tbody>
            </table>
        );
    }
}

Stats.propTypes = {
    players: PropTypes.array.isRequired,
  };

Stats.defaultProps = {
    players: [],
}
export default Stats;
