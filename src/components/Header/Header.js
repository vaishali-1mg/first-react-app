import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Header.css';
import Stats from '../Stats/Stats'; 
class Header extends Component {
    render() {
        return (
            <div className="header">
                <Stats players={this.props.players}/>
                <h1> {this.props.title}</h1>
            </div>
        );
    }
}

Header.propTypes = {
  title: PropTypes.string.isRequired,
  players: PropTypes.array.isRequired,
};
export default Header;
